import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestConstitution {
    @Test
    public void testConstitution(){
        Constitution constitution = Constitution.getConstitution("Articles");
        Constitution anotherConstitution = Constitution.getConstitution("Laws");
        assertEquals("Articles", anotherConstitution.content);
    }
}
