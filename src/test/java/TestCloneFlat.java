import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCloneFlat {
    @Test
    public void testFlat(){
        Flat flat = new Flat();
        flat.number = 12;
        flat.rooms = 3;
        flat.area = 65;
        flat.loggia = true;
        Flat anotherFlat = flat.clone();
        assertEquals(flat, anotherFlat);
    }

}
