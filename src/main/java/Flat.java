import java.util.Objects;

public class Flat {
    public int number;
    public int area;
    public int rooms;
    public boolean loggia;

    public Flat() {

    }
    public Flat(Flat flat){
        this.number = flat.number;
        this.area = flat.area;
        this.rooms = flat.rooms;
        this.loggia = flat.loggia;
    }

    public Flat clone(){
        return new Flat(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flat flat = (Flat) o;
        return number == flat.number &&
                area == flat.area &&
                rooms == flat.rooms &&
                loggia == flat.loggia;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, area, rooms, loggia);
    }
}
