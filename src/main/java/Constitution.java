public class Constitution {
    private static Constitution constitution;
    public String content;

    private Constitution(String content){
        this.content = content;
    }

    public static Constitution getConstitution(String content) {
        if (constitution == null) {
            constitution = new Constitution(content);
        }
        return constitution;
    }
}
